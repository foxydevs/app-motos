import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiPerfilTaxistaPage } from './mi-perfil-taxista.page';

describe('MiPerfilTaxistaPage', () => {
  let component: MiPerfilTaxistaPage;
  let fixture: ComponentFixture<MiPerfilTaxistaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiPerfilTaxistaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiPerfilTaxistaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
