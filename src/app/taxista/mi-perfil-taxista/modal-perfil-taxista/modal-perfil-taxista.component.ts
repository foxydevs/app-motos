import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { NotificacionService } from 'src/app/service/notificacion.service';
import { UsuarioService } from 'src/app/service/usuario.service';
import * as moment from 'moment';

@Component({
  selector: 'app-modal-perfil-taxista',
  templateUrl: './modal-perfil-taxista.component.html',
  styleUrls: ['./modal-perfil-taxista.component.scss'],
})
export class ModalPerfilTaxistaComponent implements OnInit {
  private title:string = 'Crear Cuenta';
  private passwordType:string = 'password';
  private passwordShow:boolean = false;
  private passwordType2:string = 'password';
  private passwordShow2:boolean = false;
  private passwordType3:string = 'password';
  private passwordShow3:boolean = false;
  private parameter:any;
  private disabledBtn2:boolean;
  private data = {
    username: '',
    email: '',
    password: '',
    password_repeat: '',
    nombres: '',
    apellidos: '',
    nacimiento: '',
    telefono: '',
    rol: '',
    id: ''
  }
  changePassword = {
    old_pass: '',
	  new_pass : '',
	  new_pass_rep: '',
    id: +localStorage.getItem('currentId')
  }

  constructor(
    private modalController: ModalController,
    private mainService: UsuarioService,
    private notificationService: NotificacionService,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    this.data.id = this.navParams.get('value');
    this.parameter = +this.navParams.get('edit');
    if(this.parameter == 1) {
      this.title = 'Crear Cuenta';
      this.getSingle(this.data.id)
    } else if(this.parameter == 2) {
      this.title = 'Cambiar Contraseña';
      //this.getSingle(this.data.id)
    }
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  //TOGGLE PASSWORD
  togglePassword() {
    if(this.passwordShow) {
      this.passwordShow = false;
      this.passwordType = 'password';
    } else {
      this.passwordShow = true;
      this.passwordType = 'text';
    }
  }

  //TOGGLE PASSWORD
  togglePassword2() {
    if(this.passwordShow2) {
      this.passwordShow2 = false;
      this.passwordType2 = 'password';
    } else {
      this.passwordShow2 = true;
      this.passwordType2 = 'text';
    }
  }

  //TOGGLE PASSWORD
  togglePassword3() {
    if(this.passwordShow3) {
      this.passwordShow3 = false;
      this.passwordType3 = 'password';
    } else {
      this.passwordShow3 = true;
      this.passwordType3 = 'text';
    }
  }

  //SAVE CHANGES
  saveChanges() {
    this.data.nacimiento = moment(this.data.nacimiento).format('YYYY-MM-DD');
    if(this.data.username) {
      if(this.data.email) {
        if(this.data.nombres) {
          this.update();
        } else {
          this.notificationService.alertToast("El nombre es requerido.");
        }
      } else {
        this.notificationService.alertToast("El correo electrónico es requerido.");        
      }
    } else {
      this.notificationService.alertToast("El nombre de usuario es requerido.");
    }
  }

  //CAMBIAR CONTRASEÑA
  changePasswordProfile() {
    if(this.changePassword.old_pass == this.changePassword.new_pass) {
      this.notificationService.alertToast('No se puede usar la misma contraseña.');
    } else {
      if(this.changePassword.new_pass.length >= 8) {
        if(this.changePassword.new_pass == this.changePassword.new_pass_rep) {
          this.disabledBtn2 = true;
            //UPDATE
            this.mainService.changePassword(this.changePassword)
            .subscribe((res) => {
              this.closeModal();
              this.notificationService.alertMessage('Contraseña Actualizada', 'La contraseña ha sido actualizada exitosamente.');
            }, (error) => {
              this.disabledBtn2 = false;
              this.notificationService.alertToast('Contraseña inválida.')
              console.log(this.changePassword);
              console.error(error);
            });
        } else {
          this.notificationService.alertToast('Las contraseñas no coinciden.');
        }
      } else {
        this.notificationService.alertToast('La contraseña debe contener al menos 8 caracteres.');        
      }
    }
  }

  //UPDATE
  private update() {
    this.mainService.update(this.data)
    .subscribe((res) => {
      this.closeModal();
      this.notificationService.alertMessage('Usuario actualizado', 'Su usuario ha sido actualizado exitosamente.');
    }, (error) => {
      console.log(this.data);
      console.error(error);
    });
  }

  //GET SINGLE
  getSingle(id: any) {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getSingle(id)
    .subscribe((res) => {
      this.data = res;
      this.notificationService.dismiss();
    }, (error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }
}