import { Component, OnInit } from '@angular/core';
import { ModalPerfilComponent } from 'src/app/cliente/mi-perfil-cliente/modal-perfil/modal-perfil.component';
import { ModalController } from '@ionic/angular';
import { ModalPerfilTaxistaComponent } from './modal-perfil-taxista/modal-perfil-taxista.component';

@Component({
  selector: 'app-mi-perfil-taxista',
  templateUrl: './mi-perfil-taxista.page.html',
  styleUrls: ['./mi-perfil-taxista.page.scss'],
})
export class MiPerfilTaxistaPage implements OnInit {
  private navColor:String = '1763A6';
  private table:any[];
  private data = {
    nombre: localStorage.getItem('currentFirstName') + ' '+ localStorage.getItem('currentLastName'),
    telefono: localStorage.getItem('currentCellphone'),
    email: localStorage.getItem('currentEmail'),
    picture: localStorage.getItem('currentPicture'),
    id: localStorage.getItem('currentId'),
  }

  constructor(private modalController: ModalController) { }

  ngOnInit() {
  }

  //ABRIR MODAL
  async presentModal(parameter?:number, editar?:number) {
    const modal = await this.modalController.create({
      component: ModalPerfilTaxistaComponent,
      componentProps: { 
        value: parameter,
        edit: editar,
      }
    });
    modal.onDidDismiss().then((data) => {
    });
    return await modal.present();
  }

}
