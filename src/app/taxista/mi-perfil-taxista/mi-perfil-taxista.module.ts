import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MiPerfilTaxistaPage } from './mi-perfil-taxista.page';
import { ModalPerfilTaxistaComponent } from './modal-perfil-taxista/modal-perfil-taxista.component';

const routes: Routes = [
  {
    path: '',
    component: MiPerfilTaxistaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    MiPerfilTaxistaPage,
    ModalPerfilTaxistaComponent
  ],
  entryComponents: [
    ModalPerfilTaxistaComponent
  ]
})
export class MiPerfilTaxistaPageModule {}
