import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardTaxistaPage } from './dashboard-taxista.page';

describe('DashboardTaxistaPage', () => {
  let component: DashboardTaxistaPage;
  let fixture: ComponentFixture<DashboardTaxistaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardTaxistaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardTaxistaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
