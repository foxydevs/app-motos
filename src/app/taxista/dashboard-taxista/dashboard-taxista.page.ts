import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { PujaService } from 'src/app/service/puja.service';
import { NotificacionService } from 'src/app/service/notificacion.service';
import * as moment from 'moment';

@Component({
  selector: 'app-dashboard-taxista',
  templateUrl: './dashboard-taxista.page.html',
  styleUrls: ['./dashboard-taxista.page.scss'],
})
export class DashboardTaxistaPage implements OnInit {
  private table:any[];
  private navColor:String = '1763A6';
  private itemSelected:string = 'disponibles';

  constructor(private router: Router,
    private location:Location,
    private mainService: PujaService,
    private notificationService: NotificacionService
  ) { }

  segmentChanged(ev: any) {
    this.itemSelected = ev.detail.value;
    if(this.itemSelected == 'disponibles') {
      this.getAllAvailable(0);
    } else if(this.itemSelected == 'viajes') {
      this.getAll();
    } else if(this.itemSelected == 'actual') {
      this.getAllAvailable(2);
    }
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  ngOnInit() {
    this.getAllAvailable(0);
  }

  //GET ALL
  getAllAvailable(state:number) {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getFilter(state, 'state')
    .subscribe((res) => {
      this.table = [];
      for(let x of res) {
        let data = {
          tiempo: moment(x.fecha).startOf('hour').fromNow(),
          recoger_en: x.recoger_en,
          llegar_a: x.llegar_a,
          id: x.id
        }
        this.table.push(data)
      }
      this.table.reverse();
      this.notificationService.dismiss();
    }, (error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }

  //GET ALL
  getAll() {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getFilter(+localStorage.getItem('currentId'), 'taxi')
    .subscribe((res) => {
      this.table = [];
      for(let x of res) {
        let data = {
          tiempo: moment(x.fecha).startOf('hour').fromNow(),
          recoger_en: x.recoger_en,
          llegar_a: x.llegar_a,
          id: x.id
        }
        this.table.push(data)
      }
      this.table.reverse();
      this.notificationService.dismiss();
    }, (error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }


}
