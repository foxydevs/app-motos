import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DashboardTaxistaPage } from './dashboard-taxista.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardTaxistaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DashboardTaxistaPage]
})
export class DashboardTaxistaPageModule {}
