import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { PujaService } from 'src/app/service/puja.service';
import { NotificacionService } from 'src/app/service/notificacion.service';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { RespuestaTaxistaComponent } from './respuesta-taxista/respuesta-taxista.component';
import { PujaDescripcionService } from 'src/app/service/puja-descripcion.service';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

//GOOGLE MAPS
declare var google;

@Component({
  selector: 'app-solicitudes-detalle-taxista',
  templateUrl: './solicitudes-detalle-taxista.page.html',
  styleUrls: ['./solicitudes-detalle-taxista.page.scss'],
})
export class SolicitudesDetalleTaxistaPage implements OnInit {
  private navColor:String = '1763A6';
  private data:any;
  private table:any[];
  private map:any;
  private parameter:any;
  private directionsService: any;
  private directionsDisplay: any;
  private disabledBtn:boolean;

  constructor(private router: Router,
    private location:Location,
    private activatedRoute: ActivatedRoute,
    private modalController: ModalController,
    private mainService: PujaService,
    private secondService: PujaDescripcionService,
    private notificationService: NotificacionService,
    private actionSheetController: ActionSheetController,
    private launchNavigator: LaunchNavigator) {
      this.parameter = +this.activatedRoute.snapshot.paramMap.get('id');
    }

  ngOnInit() {
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    this.getSingle(this.parameter)
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  //GET SINGLE
  getSingle(id: any) {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getSingle(id)
    .subscribe((res) => {
      if(res.state>0) {
        this.disabledBtn = true;
      }
      this.data = res;
      this.loadMap(res.recoger_en.latitud, res.recoger_en.longitud, res.llegar_a.latitud, res.llegar_a.longitud);
      this.notificationService.dismiss();
      this.getAll();
    }, (error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }

  public loadMap(lat:any, lng:any, latD:any, lngD:any){  
    let mapEle: HTMLElement = document.getElementById('map');

    let myLatLng = {lat: lat, lng: lng};

    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });
    this.directionsDisplay.setMap(this.map);

    this.calculateRoute(lat, lng, latD, lngD);
  }

  public calculateRoute(lat:any, lng:any, latD:any, lngD:any){
    this.directionsService.route({
      origin: new google.maps.LatLng(lat, lng),
      destination: new google.maps.LatLng(latD, lngD),
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING,
      avoidTolls: true
    }, (response:any, status:any)=> {
      if(status === google.maps.DirectionsStatus.OK) {
        this.directionsDisplay.setDirections(response);
      }else{
        console.log('Could not display directions due to: ' + status);
      }
    });
  }

  //ABRIR MODAL
  async presentModal() {
    const modal = await this.modalController.create({
      component: RespuestaTaxistaComponent,
      componentProps: { 
        value: this.data.id,
        cliente: this.data.cliente,
      }
    });
    modal.onDidDismiss().then((data) => {
      this.getAll();
    });
    return await modal.present();
  }

  //GET ALL
  getAll() {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.secondService.getFilter(this.data.id, 'puja')
    .subscribe((res) => {
      this.table = [];
      this.table = res;
      this.table.reverse();
      this.notificationService.dismiss();
    }, (error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }

  //UPDATE
  update() {
    let data = {
      taxi: localStorage.getItem('currentId'),
      total: this.data.total,
      state: 2,
      id: this.data.id
    }
    this.mainService.update(data)
    .subscribe((res) => {
      this.getSingle(this.parameter)
      this.notificationService.alertMessage('Solicitud Exitosa', 'Ha confirmado un viaje.');
    }, (error) => {
      this.notificationService.alertToast('Ha ocurrido un error. Intente dentro de unos minutos.');
      console.log(this.data);
      console.error(error);
    });
  }

  //UPDATE
  endTravel() {
    let data = {
      state: 1,
      id: this.data.id
    }
    this.mainService.update(data)
    .subscribe((res) => {
      this.getSingle(this.parameter)
      this.notificationService.alertMessage('Viaje Finalizado', 'Usted ha finalizado un viaje.');
    }, (error) => {
      this.notificationService.alertToast('Ha ocurrido un error. Intente dentro de unos minutos.');
      console.log(this.data);
      console.error(error);
    });
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getAll();
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  //UPDATE
  updateOffer(taxi:number, monto:number) {
    let data = {
      taxi: localStorage.getItem('currentId'),
      total: monto,
      state: 2,
      id: this.data.id
    }
    this.mainService.update(data)
    .subscribe((res) => {
      this.getSingle(this.parameter)
      this.notificationService.alertMessage('Solicitud Exitosa', 'Ha confirmado un viaje.');
    }, (error) => {
      this.notificationService.alertToast('Ha ocurrido un error. Intente dentro de unos minutos.');
      console.log(this.data);
      console.error(error);
    });
  }

  async getLocation(lat:any, lng:any, latD:any, lngD:any) {
    const actionSheet = await this.actionSheetController.create({
      header: 'App de Ubicación',
      buttons: [
      {
        text: 'Google Maps',
        icon: 'map',
        handler: () => {
          let options: LaunchNavigatorOptions = {
            start: `${lat},${lng}`,
            app: this.launchNavigator.APP.GOOGLE_MAPS
          }
          
          this.launchNavigator.navigate([latD, lngD], options)
          .then(
            success => console.log('Launched navigator'),
            error => console.log('Error launching navigator', error)
          );
        }
      }, {
        text: 'Waze',
        icon: 'pin',
        handler: () => {
          let options: LaunchNavigatorOptions = {
            start: `${lat},${lng}`,
            app: this.launchNavigator.APP.WAZE
          }
          
          this.launchNavigator.navigate([latD, lngD], options)
          .then(
            success => console.log('Launched navigator'),
            error => console.log('Error launching navigator', error)
          );
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

}
