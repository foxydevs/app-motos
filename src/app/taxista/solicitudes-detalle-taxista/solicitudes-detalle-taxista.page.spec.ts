import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudesDetalleTaxistaPage } from './solicitudes-detalle-taxista.page';

describe('SolicitudesDetalleTaxistaPage', () => {
  let component: SolicitudesDetalleTaxistaPage;
  let fixture: ComponentFixture<SolicitudesDetalleTaxistaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudesDetalleTaxistaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesDetalleTaxistaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
