import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SolicitudesDetalleTaxistaPage } from './solicitudes-detalle-taxista.page';
import { RespuestaTaxistaComponent } from './respuesta-taxista/respuesta-taxista.component';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';

const routes: Routes = [
  {
    path: '',
    component: SolicitudesDetalleTaxistaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    SolicitudesDetalleTaxistaPage,
    RespuestaTaxistaComponent
  ],
  entryComponents: [
    RespuestaTaxistaComponent
  ],
  providers: [
    LaunchNavigator
  ]
})
export class SolicitudesDetalleTaxistaPageModule {}
