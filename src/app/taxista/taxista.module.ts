import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([
    { path: '', 
      loadChildren: './dashboard-taxista/dashboard-taxista.module#DashboardTaxistaPageModule'
    },
    { path: 'mis-taxis-taxista', 
      loadChildren: './mis-taxis-taxista/mis-taxis-taxista.module#MisTaxisTaxistaPageModule'
    },
    { path: 'solicitudes-detalle-taxista/:id', 
      loadChildren: './solicitudes-detalle-taxista/solicitudes-detalle-taxista.module#SolicitudesDetalleTaxistaPageModule' 
    },
    { path: 'mi-perfil-taxista', 
      loadChildren: './mi-perfil-taxista/mi-perfil-taxista.module#MiPerfilTaxistaPageModule'
    },
    ])
  ]
})
export class TaxistaModule { }
