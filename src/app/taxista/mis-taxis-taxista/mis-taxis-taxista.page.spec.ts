import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisTaxisTaxistaPage } from './mis-taxis-taxista.page';

describe('MisTaxisTaxistaPage', () => {
  let component: MisTaxisTaxistaPage;
  let fixture: ComponentFixture<MisTaxisTaxistaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisTaxisTaxistaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisTaxisTaxistaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
