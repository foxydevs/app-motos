import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { NotificacionService } from 'src/app/service/notificacion.service';
import { TaxisService } from 'src/app/service/taxis.service';

@Component({
  selector: 'app-modal-taxi',
  templateUrl: './modal-taxi.component.html',
  styleUrls: ['./modal-taxi.component.scss'],
})
export class ModalTaxiComponent implements OnInit {
  private title:string = 'Registro de Taxi';
  private appColor:string = '1763A6';
  private data = {
    id: '',
    matricula: '',
    marca: '',
    linea: '',
    codigo: '',
    duenio: localStorage.getItem('currentId'),
  }
  constructor(
    private modalController: ModalController,
    private mainService: TaxisService,
    private notificationService: NotificacionService,
    private navParams: NavParams
  ) {
    this.data.id = this.navParams.get('value');
  }

  ngOnInit() {
    //this.getAll(+localStorage.getItem('currentId'))
    if(this.data.id) {
      this.getSingle(this.data.id);
    }
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  //GET SINGLE
  getSingle(id: any) {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getSingle(id)
    .subscribe((res) => {
      this.data = res;
      this.notificationService.dismiss();
    }, (error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    if(this.data.matricula) {
      if(this.data.marca) {
        if(this.data.id) {
          this.update();
        } else {
          this.create();
        }
      } else {
        this.notificationService.alertToast("La marca es requerida.");
      }
    } else {
      this.notificationService.alertToast("La matrícula es requerida.");
    }
  }

  //CREATE
  private create() {
    this.mainService.create(this.data)
    .subscribe((res) => {
      this.modalController.dismiss(res);
      this.notificationService.alertMessage('Taxi Agregado', 'El taxi ha sido agregado exitosamente.');
    }, (error) => {
      console.log(this.data);
      console.error(error);
    });
  }

  //UPDATE
  private update() {
    this.mainService.update(this.data)
    .subscribe((res) => {
      this.modalController.dismiss(res);
      this.notificationService.alertMessage('Taxi Actualizado', 'El taxi ha sido actualizado exitosamente.');
    }, (error) => {
      console.log(this.data);
      console.error(error);
    });
  }
}
