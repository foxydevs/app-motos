import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalTaxiComponent } from './modal-taxi/modal-taxi.component';
import { TaxisService } from 'src/app/service/taxis.service';
import { NotificacionService } from 'src/app/service/notificacion.service';

@Component({
  selector: 'app-mis-taxis-taxista',
  templateUrl: './mis-taxis-taxista.page.html',
  styleUrls: ['./mis-taxis-taxista.page.scss'],
})
export class MisTaxisTaxistaPage implements OnInit {
  private navColor:String = '1763A6';
  private table:any[];
  constructor(private modalController: ModalController,
    private mainService: TaxisService,
    private notificationService: NotificacionService) { }

  ngOnInit() {
    this.getAll(+localStorage.getItem('currentId'));
  }

  //GET ALL
  getAll(id:number) {
    this.mainService.getFilter(id, 'usuario')
    .subscribe((res) => {
      this.table = [];
      this.table = res;
    }, (error) => {
      console.log(error);
    })
  }

  //ABRIR MODAL
  async presentModal(parameter?:number) {
    const modal = await this.modalController.create({
      component: ModalTaxiComponent,
      componentProps: { 
        value: parameter
      }
    });
    modal.onDidDismiss().then((data) => {
      console.log(data)
      this.getAll(+localStorage.getItem('currentId'));
    });
    return await modal.present();
  }

  //DELETE
  delete(id:number) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Taxi Eliminado', 'El taxi ha sido eliminado exitosamente.');
      this.getAll(+localStorage.getItem('currentId'))
    }, (error) => {
      console.error(error);
    });
  }

}
