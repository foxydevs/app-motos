import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MisTaxisTaxistaPage } from './mis-taxis-taxista.page';
import { ModalTaxiComponent } from './modal-taxi/modal-taxi.component';
import { TaxisService } from 'src/app/service/taxis.service';

const routes: Routes = [
  {
    path: '',
    component: MisTaxisTaxistaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    MisTaxisTaxistaPage,
    ModalTaxiComponent
  ],
  entryComponents: [
    ModalTaxiComponent
  ],
  providers: [
    TaxisService
  ]
})
export class MisTaxisTaxistaPageModule {}
