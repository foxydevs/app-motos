import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router';
import { NavController, Events } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class HomeGuard implements CanActivate {
  
  constructor(
    private navCtrl: NavController,
    private events: Events
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('currentUser')) {
      if(localStorage.getItem('currentRol') == '2') {
        this.navCtrl.navigateForward('taxi');
          this.events.publish('user:taxi');
      } else {
        this.navCtrl.navigateForward('cliente');
        this.events.publish('user:cliente');
      }
    }

    if (!localStorage.getItem('currentUser')) {
      return true;
    }
  }

}
