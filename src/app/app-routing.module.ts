import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';
import { HomeGuard } from './guard/home.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'cliente', loadChildren: './cliente/cliente.module#ClienteModule', canActivate: [AuthGuard] },
  { path: 'taxi', loadChildren: './taxista/taxista.module#TaxistaModule', canActivate: [AuthGuard] },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule', canActivate: [HomeGuard] },


  //{ path: 'restaurantes-cliente', loadChildren: './cliente/restaurantes-cliente/restaurantes-cliente.module#RestaurantesClientePageModule' },



  //{ path: 'mis-direcciones-cliente', loadChildren: './cliente/mis-direcciones-cliente/mis-direcciones-cliente.module#MisDireccionesClientePageModule' },

  /*{ path: 'mis-viajes', loadChildren: './home/mis-viajes/mis-viajes.module#MisViajesPageModule' },
  { path: 'mi-cuenta', loadChildren: './home/mi-cuenta/mi-cuenta.module#MiCuentaPageModule' },*/
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
