import { Component } from '@angular/core';

import { Platform, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [];
  authentication:string = localStorage.getItem('currentAuthentication');
  private data = {
    nombre: localStorage.getItem('currentFirstName') + ' '+ localStorage.getItem('currentLastName'),
    foto: localStorage.getItem('currentPicture'),
    email: localStorage.getItem('currentEmail'),
    portada: 'http://1-byte.co.uk/wp-content/uploads/2017/05/https-designguidelines.withgoogle.com-android-wear-assets-0B6EFxKnH3IOMY1lpQTFma0sxUVk-materialdesign-introduction.png'
  }

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private events: Events,
  ) {
    this.initializeApp();
    if(!localStorage.getItem('currentAuthentication')) {
      this.authentication = 'NoAuthentication'
    }
    if(localStorage.getItem('currentRol') == '2') {
      this.getModuleTaxista();
    } else if(localStorage.getItem('currentRol') == '1' || localStorage.getItem('currentRol') == '3') {
      this.getModuleCliente();
    }
    events.subscribe('user:client', res => {
      this.getModuleCliente();
    });
    events.subscribe('user:taxi', res => {
      this.getModuleTaxista();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  public getModuleCliente() {
    this.authentication = localStorage.getItem('currentAuthentication');
    this.getData();
    this.appPages = [];
    this.appPages.push({ title: 'Inicio', url: '/cliente', icon: 'home', color: '1763A6'});
    this.appPages.push({ title: 'Relizar Pedido', url: '/cliente/pedidos-cliente', icon: 'bicycle', color: '1763A6'});
    this.appPages.push({ title: 'Restaurantes', url: '/cliente/categoria-cliente/restaurante', icon: 'pizza', color: '1763A6'});
    this.appPages.push({ title: 'Categoría', url: '/cliente/categoria-cliente/categoria', icon: 'apps', color: '1763A6'});
    this.appPages.push({ title: 'Mi Carrito', url: '/cliente/carrito/0', icon: 'cart', color: '1763A6'});
    //this.appPages.push({ title: 'Dashboard Test', url: '/cliente/dashboard-test', icon: 'home', color: '1763A6'});
    this.appPages.push({ title: 'Tus Viajes', url: '/cliente/mis-viajes-cliente', icon: 'briefcase', color: '1763A6'});
    this.appPages.push({ title: 'Tus Solicitudes', url: '/cliente/mis-solicitudes-cliente', icon: 'pricetags', color: '1763A6'});
    this.appPages.push({ title: 'Configuración', url: '/cliente/mi-perfil-cliente', icon: 'settings', color: '1763A6'});
  }

  public getModuleTaxista() {
    this.authentication = localStorage.getItem('currentAuthentication');
    this.getData();
    this.appPages = [];
    this.appPages.push({ title: 'Inicio', url: '/taxi', icon: 'home', color: '1763A6'});
    this.appPages.push({ title: 'Solicitudes', url: '/taxi/solicitudes-taxista', icon: 'pricetags', color: '1763A6'});
    this.appPages.push({ title: 'Mis Motos', url: '/taxi/mis-taxis-taxista', icon: 'bicycle', color: '1763A6'});
    this.appPages.push({ title: 'Configuración', url: '/taxi/mi-perfil-taxista', icon: 'settings', color: '1763A6'});

  }

  //GO TO ROUTE
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  //CERRAR SESIÓN
  logOut() {
    localStorage.clear();
    this.getData();
    localStorage.setItem('currentAuthentication', 'NoAuthentication');
    this.authentication = localStorage.getItem('currentAuthentication');
    this.appPages = [];
    this.goToRoute('login');
  }

  //GET DATA
  getData() {
      this.data.nombre = localStorage.getItem('currentFirstName') + ' '+ localStorage.getItem('currentLastName'),
      this.data.foto = localStorage.getItem('currentPicture'),
      this.data.email = localStorage.getItem('currentEmail'),
      this.data.portada= 'http://1-byte.co.uk/wp-content/uploads/2017/05/https-designguidelines.withgoogle.com-android-wear-assets-0B6EFxKnH3IOMY1lpQTFma0sxUVk-materialdesign-introduction.png'
  }
}
