import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController, Events } from '@ionic/angular';
import { ModalRegistroComponent } from './modal-registro/modal-registro.component';
import { NotificacionService } from '../service/notificacion.service';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private disabledBtn:boolean = false;
  private passwordType:string = 'password';
  private passwordShow:boolean = false;
  private data = {
    username: '',
    password: ''
  }

  constructor(private router: Router,
    private location:Location,
    private mainService: AuthService,
    private notificacionService: NotificacionService,
    public events: Events,
    private modalController: ModalController) { }

  ngOnInit() {
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  //TOGGLE PASSWORD
  togglePassword() {
    if(this.passwordShow) {
      this.passwordShow = false;
      this.passwordType = 'password';
    } else {
      this.passwordShow = true;
      this.passwordType = 'text';
    }
  }

  authentication() {
    //this.goToRoute('cliente');
    if(this.data.username) {
      if(this.data.password) {
        this.auth();
      } else {
        this.notificacionService.alertToast("La contraseña es requerida.");
      }
    } else {
      this.notificacionService.alertToast("El usuario es requerido.");
    }
  }

  auth() {
    let events = this.events;
    this.disabledBtn = true;
    this.mainService.auth(this.data)
    .subscribe((res) => {
      localStorage.setItem('currentUser', res.username);
      localStorage.setItem('currentId', res.id);
      localStorage.setItem('currentFirstName', res.nombres);
      localStorage.setItem('currentLastName', res.apellidos);      
      localStorage.setItem('currentEmail', res.email);
      localStorage.setItem('currentState', res.state);
      localStorage.setItem('currentCellphone', res.telefono);
      localStorage.setItem('currentRol', res.rol);
      localStorage.setItem('currentCarrito', JSON.stringify([]));
      if(res.foto) {
        localStorage.setItem('currentPicture', res.foto);
      } else {
        localStorage.setItem('currentPicture', 'https://scontent.flim5-3.fna.fbcdn.net/v/t1.0-9/35271090_906761809506001_4717199595224956928_n.jpg?_nc_cat=107&_nc_ht=scontent.flim5-3.fna&oh=eb27ad44783256d7b02b2cedd2d034b1&oe=5D11A9A6');
      }
      switch(+res.rol) {
        case 2:
          localStorage.setItem('currentAuthentication', 'Authentication');
          events.publish('user:taxi');
          this.goToRoute('taxi');
        break;
        case 3:
          localStorage.setItem('currentAuthentication', 'Authentication');
          events.publish('user:client');
          this.goToRoute('cliente');
        break;
        default:
          localStorage.setItem('currentAuthentication', 'Authentication');
          events.publish('user:client');
          this.goToRoute('cliente');
        break;
      }
      this.disabledBtn = false;
    }, (error) => {
      console.log(this.data)
      console.error(error);
      this.disabledBtn = false;
      this.notificacionService.alertToast("Usuario o contraseña incorrectos.");
    });
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalRegistroComponent
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
    });
    return await modal.present();
  }

}
