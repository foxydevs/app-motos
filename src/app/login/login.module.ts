import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoginPage } from './login.page';
import { ModalRegistroComponent } from './modal-registro/modal-registro.component';
import { UsuarioService } from '../service/usuario.service';
import { NotificacionService } from '../service/notificacion.service';

const routes: Routes = [
  {
    path: '',
    component: LoginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    LoginPage,
    ModalRegistroComponent
  ],
  entryComponents: [
    ModalRegistroComponent
  ],
  providers: [
    UsuarioService,
    NotificacionService
  ]
})
export class LoginPageModule {}
