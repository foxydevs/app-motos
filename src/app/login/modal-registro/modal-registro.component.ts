import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UsuarioService } from 'src/app/service/usuario.service';
import { NotificacionService } from 'src/app/service/notificacion.service';
import * as moment from 'moment';

@Component({
  selector: 'app-modal-registro',
  templateUrl: './modal-registro.component.html',
  styleUrls: ['./modal-registro.component.scss'],
})
export class ModalRegistroComponent implements OnInit {
  private title:string = 'Crear Cuenta';
  private passwordType:string = 'password';
  private passwordShow:boolean = false;
  private passwordType2:string = 'password';
  private passwordShow2:boolean = false;
  private data = {
    username: '',
    email: '',
    password: '',
    password_repeat: '',
    nombres: '',
    apellidos: '',
    nacimiento: '',
    telefono: '',
    rol: ''
  }

  constructor(
    private modalController: ModalController,
    private mainService: UsuarioService,
    private notificationService: NotificacionService,
  ) { }

  ngOnInit() {}

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  //TOGGLE PASSWORD
  togglePassword() {
    if(this.passwordShow) {
      this.passwordShow = false;
      this.passwordType = 'password';
    } else {
      this.passwordShow = true;
      this.passwordType = 'text';
    }
  }

  //TOGGLE PASSWORD
  togglePassword2() {
    if(this.passwordShow2) {
      this.passwordShow2 = false;
      this.passwordType2 = 'password';
    } else {
      this.passwordShow2 = true;
      this.passwordType2 = 'text';
    }
  }

  //SAVE CHANGES
  saveChanges() {
    this.data.nacimiento = moment(this.data.nacimiento).format('YYYY-MM-DD');
    if(this.data.username) {
      if(this.data.email) {
        if(this.data.password) {
          if(this.data.password == this.data.password_repeat) {
            if(this.data.nombres) {
              this.create();
            } else {
              this.notificationService.alertToast("El nombre es requerido.");
            }
          } else {
            this.notificationService.alertToast("Las contraseñas no son iguales.");
          }
        } else {
          this.notificationService.alertToast("La contraseña es requerida.");        
        }
      } else {
        this.notificationService.alertToast("El correo electrónico es requerido.");        
      }
    } else {
      this.notificationService.alertToast("El nombre de usuario es requerido.");
    }
  }

  //CREATE
  private create() {
    this.mainService.create(this.data)
    .subscribe((res) => {
      this.closeModal();
      this.notificationService.alertMessage('Usuario registrado', 'Su registro fue realizado con exito, por favor iniciar sesión.');
    }, (error) => {
      console.log(this.data);
      console.error(error);
    });
  }
}
