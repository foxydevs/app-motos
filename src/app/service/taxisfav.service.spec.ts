import { TestBed } from '@angular/core/testing';

import { TaxisfavService } from './taxisfav.service';

describe('TaxisfavService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TaxisfavService = TestBed.get(TaxisfavService);
    expect(service).toBeTruthy();
  });
});
