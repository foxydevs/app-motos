import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalDireccionesComponent } from './modal-direcciones/modal-direcciones.component';
import { DireccionService } from 'src/app/service/direccion.service';
import { NotificacionService } from 'src/app/service/notificacion.service';
import { ModalPerfilComponent } from './modal-perfil/modal-perfil.component';

@Component({
  selector: 'app-mi-perfil-cliente',
  templateUrl: './mi-perfil-cliente.page.html',
  styleUrls: ['./mi-perfil-cliente.page.scss'],
})
export class MiPerfilClientePage implements OnInit {
  private navColor:String = '1763A6';
  private table:any[];
  private data = {
    nombre: localStorage.getItem('currentFirstName') + ' '+ localStorage.getItem('currentLastName'),
    telefono: localStorage.getItem('currentCellphone'),
    email: localStorage.getItem('currentEmail'),
    picture: localStorage.getItem('currentPicture'),
    id: localStorage.getItem('currentId'),
  }

  constructor(private modalController: ModalController,
    private mainService: DireccionService,
    private notificationService: NotificacionService
    ) { }

  ngOnInit() {
    this.getAll(+localStorage.getItem('currentId'));
  }

  //ABRIR MODAL
  async presentModal(parameter?:number) {
    const modal = await this.modalController.create({
      component: ModalDireccionesComponent,
      componentProps: { 
        value: parameter
      }
    });
    modal.onDidDismiss().then((data) => {
      if(data) {
        this.getAll(+localStorage.getItem('currentId'));
      }
    });
    return await modal.present();
  }

  //ABRIR MODAL
  async openModal(parameter?:number, editar?:number) {
    const modal = await this.modalController.create({
      component: ModalPerfilComponent,
      componentProps: { 
        value: parameter,
        edit: editar,
      }
    });
    modal.onDidDismiss().then((data) => {
      if(data) {
        this.getAll(+localStorage.getItem('currentId'));
      }
    });
    return await modal.present();
  }

  getAll(id:number) {
    this.mainService.getFilter(id, 'usuario')
    .subscribe((res) => {
      this.table = [];
      this.table = res;
    }, (error) => {
      console.log(error);
    })
  }

  //DELETE
  delete(id:number) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Dirección Eliminada', 'La dirección ha sido eliminada exitosamente.');
      this.getAll(+localStorage.getItem('currentId'))
    }, (error) => {
      console.error(error);
    });
  }

}
