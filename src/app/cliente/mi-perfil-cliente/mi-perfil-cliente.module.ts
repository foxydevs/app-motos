import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MiPerfilClientePage } from './mi-perfil-cliente.page';
import { ModalDireccionesComponent } from './modal-direcciones/modal-direcciones.component';
import { ModalPerfilComponent } from './modal-perfil/modal-perfil.component';

const routes: Routes = [
  {
    path: '',
    component: MiPerfilClientePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    MiPerfilClientePage,
    ModalDireccionesComponent,
    ModalPerfilComponent
  ], entryComponents: [
    ModalDireccionesComponent,
    ModalPerfilComponent
  ]
})
export class MiPerfilClientePageModule {}
