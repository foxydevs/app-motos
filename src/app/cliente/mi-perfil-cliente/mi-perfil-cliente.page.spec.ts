import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiPerfilClientePage } from './mi-perfil-cliente.page';

describe('MiPerfilClientePage', () => {
  let component: MiPerfilClientePage;
  let fixture: ComponentFixture<MiPerfilClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiPerfilClientePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiPerfilClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
