import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { DireccionService } from 'src/app/service/direccion.service';
import { NotificacionService } from 'src/app/service/notificacion.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

//GOOGLE
declare var google;

@Component({
  selector: 'app-modal-direcciones',
  templateUrl: './modal-direcciones.component.html',
  styleUrls: ['./modal-direcciones.component.scss'],
})
export class ModalDireccionesComponent implements OnInit {
  private title:string = 'Crear Dirección';
  private appColor:string = '1763A6';
  private map: any;

  private data = {
    direccion: '',
    usuario: localStorage.getItem('currentId'),
    tipo: 0,
    latitud: 0,
    longitud: 0,
    titulo: '',
    id: ''
  }

  constructor(
    private modalController: ModalController,
    private mainService: DireccionService,
    private notificationService: NotificacionService,
    private geolocation: Geolocation,
    private navParams: NavParams
  ) {
    this.data.id = this.navParams.get('value');
  }

  ngOnInit() {
    if(this.data.id) {
      this.getSingle(this.data.id);
    } else {
      this.getLocation();
    }
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  //SAVE CHANGES
  saveChanges() {
    if(this.data.titulo) {
      if(this.data.direccion) {
        if(this.data.tipo) {
          if(this.data.id) {
            this.update()         
          } else {
            this.create();
          }
        } else {
          this.notificationService.alertToast("El tipo es requerido.");          
        }
      } else {
        this.notificationService.alertToast("La dirección es requerida.");
      }
    } else {
      this.notificationService.alertToast("El titulo es requerido.");
    }    
  }

  //CREATE
  private create() {
    this.mainService.create(this.data)
    .subscribe((res) => {
      this.modalController.dismiss(res);
      this.notificationService.alertMessage('Dirección Agregada', 'La dirección ha sido agregada exitosamente.');
    }, (error) => {
      console.log(this.data);
      console.error(error);
    });
  }

  //UPDATE
  private update() {
    this.mainService.update(this.data)
    .subscribe((res) => {
      this.modalController.dismiss(res);
      this.notificationService.alertMessage('Dirección Actualizada', 'La dirección ha sido actualizada exitosamente.');
    }, (error) => {
      console.log(this.data);
      console.error(error);
    });
  }

  //GET SINGLE
  getSingle(id: any) {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getSingle(id)
    .subscribe((res) => {
      this.data = res;
      this.loadMap(res.latitud, res.longitud)
      this.notificationService.dismiss();
    }, (error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }

  public loadMap(lat:any, long:any){  
    let mapEle: HTMLElement = document.getElementById('map');

    let myLatLng = {lat: lat, lng: long};

    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    var marker:any;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.data.latitud = evt.latLng.lat();
      this.data.longitud = evt.latLng.lng();
    });
  }

  getLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.data.latitud = resp.coords.latitude
      this.data.longitud = resp.coords.longitude
      this.loadMap(resp.coords.latitude, resp.coords.longitude);
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }

}
