import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisViajesClientePage } from './mis-viajes-cliente.page';

describe('MisViajesClientePage', () => {
  let component: MisViajesClientePage;
  let fixture: ComponentFixture<MisViajesClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisViajesClientePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisViajesClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
