import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MisViajesClientePage } from './mis-viajes-cliente.page';

const routes: Routes = [
  {
    path: '',
    component: MisViajesClientePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MisViajesClientePage]
})
export class MisViajesClientePageModule {}
