import { Component, OnInit } from '@angular/core';
import { NotificacionService } from 'src/app/service/notificacion.service';
import { PujaService } from 'src/app/service/puja.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-mis-viajes-cliente',
  templateUrl: './mis-viajes-cliente.page.html',
  styleUrls: ['./mis-viajes-cliente.page.scss'],
})
export class MisViajesClientePage implements OnInit {
  private navColor:String = '1763A6';
  private table:any[];

  constructor(private router: Router,
    private location:Location,
    private mainService: PujaService,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.getAll(+localStorage.getItem('currentId'))
  }

  
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  //GET ALL
  getAll(id: any) {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getFilter(id, 'cliente')
    .subscribe((res) => {
      this.table = [];
      for(let x of res) {
        let data = {
          recoger_en: x.recoger_en,
          llegar_a: x.llegar_a,
          id: x.id,
          state: x.state
        }
        this.table.push(data)
      }
      this.table.reverse();
      this.notificationService.dismiss();
    }, (error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }

}
