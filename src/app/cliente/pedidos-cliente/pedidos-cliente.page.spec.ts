import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidosClientePage } from './pedidos-cliente.page';

describe('PedidosClientePage', () => {
  let component: PedidosClientePage;
  let fixture: ComponentFixture<PedidosClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidosClientePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidosClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
