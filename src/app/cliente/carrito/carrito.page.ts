import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.page.html',
  styleUrls: ['./carrito.page.scss'],
})
export class CarritoPage implements OnInit {

  private navColor:String = '1763A6';
  private parameter:any;
  private productos:any[];

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location) {
      this.parameter = this.activatedRoute.snapshot.paramMap.get('id');
    }

  ngOnInit() {
    this.productos = JSON.parse(localStorage.getItem('currentCarrito'));
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  delete(e:any) {
    this.productos.splice(this.productos.indexOf(e),1)
    localStorage.removeItem('currentCarrito');
    localStorage.setItem('currentCarrito', JSON.stringify(this.productos))
  }

}
