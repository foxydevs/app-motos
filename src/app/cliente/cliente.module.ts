import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([
    { path: '', 
      loadChildren: './dashboard-cliente/dashboard-cliente.module#DashboardClientePageModule'
    },
    { path: 'mi-perfil-cliente', 
      loadChildren: './mi-perfil-cliente/mi-perfil-cliente.module#MiPerfilClientePageModule' 
    },
    { path: 'mis-viajes-cliente', 
      loadChildren: './mis-viajes-cliente/mis-viajes-cliente.module#MisViajesClientePageModule' 
    },
    { path: 'mis-solicitudes-cliente', 
      loadChildren: './mis-solicitudes-cliente/mis-solicitudes-cliente.module#MisSolicitudesClientePageModule'
    },
    { path: 'mis-solicitudes-detalle-cliente/:id', 
      loadChildren: './mis-solicitudes-detalle-cliente/mis-solicitudes-detalle-cliente.module#MisSolicitudesDetalleClientePageModule' 
    },
    { path: 'dashboard-test', 
      loadChildren: './dashboard-test/dashboard-test.module#DashboardTestPageModule' 
    },
    { path: 'pedidos-cliente', 
      loadChildren: './pedidos-cliente/pedidos-cliente.module#PedidosClientePageModule' 
    },
    { path: 'categoria-cliente/:type', 
      loadChildren: './categoria-cliente/categoria-cliente.module#CategoriaClientePageModule' 
    },
    { path: 'producto/:id', 
      loadChildren: './producto/producto.module#ProductoPageModule'
    },
    { path: 'carrito/:id', 
      loadChildren: './carrito/carrito.module#CarritoPageModule' 
    },
    ])
  ]
})
export class ClienteModule { }
