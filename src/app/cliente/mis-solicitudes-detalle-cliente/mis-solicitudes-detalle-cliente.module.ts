import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MisSolicitudesDetalleClientePage } from './mis-solicitudes-detalle-cliente.page';
import { RespuestaClienteComponent } from './respuesta-cliente/respuesta-cliente.component';

const routes: Routes = [
  {
    path: '',
    component: MisSolicitudesDetalleClientePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    MisSolicitudesDetalleClientePage,
    RespuestaClienteComponent
  ],
  entryComponents:[
    RespuestaClienteComponent
  ]
})
export class MisSolicitudesDetalleClientePageModule {}
