import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PujaService } from 'src/app/service/puja.service';
import { ModalController } from '@ionic/angular';
import { PujaDescripcionService } from 'src/app/service/puja-descripcion.service';
import { NotificacionService } from 'src/app/service/notificacion.service';
import { Location } from '@angular/common';
import { RespuestaClienteComponent } from './respuesta-cliente/respuesta-cliente.component';

//GOOGLE MAPS
declare var google;

@Component({
  selector: 'app-mis-solicitudes-detalle-cliente',
  templateUrl: './mis-solicitudes-detalle-cliente.page.html',
  styleUrls: ['./mis-solicitudes-detalle-cliente.page.scss'],
})
export class MisSolicitudesDetalleClientePage implements OnInit {
  private navColor:String = '1763A6';
  private data:any;
  private table:any[];
  private map:any;
  private parameter:any;
  private directionsService: any;
  private directionsDisplay: any;

  constructor(private router: Router,
    private location:Location,
    private activatedRoute: ActivatedRoute,
    private modalController: ModalController,
    private mainService: PujaService,
    private secondService: PujaDescripcionService,
    private notificationService: NotificacionService) {
      this.parameter = +this.activatedRoute.snapshot.paramMap.get('id');
    }

  ngOnInit() {
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    this.getSingle(this.parameter)
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  //GET SINGLE
  getSingle(id: any) {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getSingle(id)
    .subscribe((res) => {
      this.data = res;
      this.loadMap(res.recoger_en.latitud, res.recoger_en.longitud, res.llegar_a.latitud, res.llegar_a.longitud);
      this.getAll();
      this.notificationService.dismiss();
    }, (error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }

  public loadMap(lat:any, lng:any, latD:any, lngD:any){  
    let mapEle: HTMLElement = document.getElementById('map');

    let myLatLng = {lat: lat, lng: lng};

    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });
    this.directionsDisplay.setMap(this.map);

    this.calculateRoute(lat, lng, latD, lngD);
  }

  public calculateRoute(lat:any, lng:any, latD:any, lngD:any){
    this.directionsService.route({
      origin: new google.maps.LatLng(lat, lng),
      destination: new google.maps.LatLng(latD, lngD),
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING,
      avoidTolls: true
    }, (response:any, status:any)=> {
      if(status === google.maps.DirectionsStatus.OK) {
        this.directionsDisplay.setDirections(response);
      }else{
        console.log('Could not display directions due to: ' + status);
      }
    });
  }

  //ABRIR MODAL
  async presentModal() {
    const modal = await this.modalController.create({
      component: RespuestaClienteComponent,
      componentProps: { 
        value: this.data.id,
        cliente: this.data.cliente,
      }
    });
    modal.onDidDismiss().then((data) => {
      console.log(data)
      this.getAll();
    });
    return await modal.present();
  }

  //GET ALL
  getAll() {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.secondService.getFilter(this.data.id, 'puja')
    .subscribe((res) => {
      this.table = [];
      this.table = res;
      this.table.reverse();
      this.notificationService.dismiss();
    }, (error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }

  //UPDATE
  update(taxi:number, monto:number) {
    let data = {
      taxi: taxi,
      total: monto,
      state: 2,
      id: this.data.id
    }
    this.mainService.update(data)
    .subscribe((res) => {
      this.modalController.dismiss(res);
      this.notificationService.alertMessage('Solicitud Exitosa', 'El taxi que solicito estará llegando en unos minutos.');
    }, (error) => {
      console.log(this.data);
      console.error(error);
    });
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getAll();
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

}
