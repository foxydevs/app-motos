import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { PujaDescripcionService } from 'src/app/service/puja-descripcion.service';
import { NotificacionService } from 'src/app/service/notificacion.service';

@Component({
  selector: 'app-respuesta-cliente',
  templateUrl: './respuesta-cliente.component.html',
  styleUrls: ['./respuesta-cliente.component.scss'],
})
export class RespuestaClienteComponent implements OnInit {
  private title:string = 'Registro de Taxi';
  private appColor:string = '1763A6';
  private data = {
    cliente: '',
    taxi: localStorage.getItem('currentId'),
    puja: '',
    respuesta: '',
    monto: '',
    state: 2
  }
  constructor(
    private modalController: ModalController,
    private mainService: PujaDescripcionService,
    private notificationService: NotificacionService,
    private navParams: NavParams
  ) {
    this.data.puja = this.navParams.get('value');
    this.data.cliente = this.navParams.get('cliente');
  }

  ngOnInit() {
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  //GET SINGLE
  getSingle(id: any) {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getSingle(id)
    .subscribe((res) => {
      this.data = res;
      this.notificationService.dismiss();
    }, (error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    if(this.data.monto) {
      this.create();
    } else {
      this.notificationService.alertToast('El monto es requerido.');
    }
  }

  //CREATE
  private create() {
    this.mainService.create(this.data)
    .subscribe((res) => {
      this.modalController.dismiss(res);
      this.notificationService.alertMessage('Respuesta Enviada', 'Su respuesta a sido enviada espere por favor.');
    }, (error) => {
      console.log(this.data);
      console.error(error);
    });
  }

}
