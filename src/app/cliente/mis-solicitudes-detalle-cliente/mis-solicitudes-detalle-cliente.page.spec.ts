import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisSolicitudesDetalleClientePage } from './mis-solicitudes-detalle-cliente.page';

describe('MisSolicitudesDetalleClientePage', () => {
  let component: MisSolicitudesDetalleClientePage;
  let fixture: ComponentFixture<MisSolicitudesDetalleClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisSolicitudesDetalleClientePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisSolicitudesDetalleClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
