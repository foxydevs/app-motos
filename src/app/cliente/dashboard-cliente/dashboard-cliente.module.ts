import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { DashboardClientePage } from './dashboard-cliente.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ModalMisDireccionesComponent } from './modal-mis-direcciones/modal-mis-direcciones.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardClientePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    DashboardClientePage,
    ModalMisDireccionesComponent
  ],
  entryComponents: [
    ModalMisDireccionesComponent
  ],
  providers: [Geolocation]
})
export class DashboardClientePageModule {}
