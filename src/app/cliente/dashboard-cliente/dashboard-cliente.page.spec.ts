import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardClientePage } from './dashboard-cliente.page';

describe('DashboardClientePage', () => {
  let component: DashboardClientePage;
  let fixture: ComponentFixture<DashboardClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardClientePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
