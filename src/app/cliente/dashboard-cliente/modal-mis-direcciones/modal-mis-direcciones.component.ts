import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DireccionService } from 'src/app/service/direccion.service';
import { NotificacionService } from 'src/app/service/notificacion.service';

@Component({
  selector: 'app-modal-mis-direcciones',
  templateUrl: './modal-mis-direcciones.component.html',
  styleUrls: ['./modal-mis-direcciones.component.scss'],
})
export class ModalMisDireccionesComponent implements OnInit {
  private table:any[];
  private title:string = 'Mis direcciones';
  private appColor:string = '1763A6';

  constructor(
    private modalController: ModalController,
    private mainService: DireccionService,
    private notificationService: NotificacionService,
  ) {
  }

  ngOnInit() {
    this.getAll(+localStorage.getItem('currentId'))
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  //GET SINGLE
  getAll(id: any) {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getFilter(id, 'usuario')
    .subscribe((res) => {
      this.table = [];
      this.table = res;
      this.notificationService.dismiss();
    }, (error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }

  getAddress(e:any) {
    this.modalController.dismiss(e)
  }

}
