import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MisSolicitudesClientePage } from './mis-solicitudes-cliente.page';

const routes: Routes = [
  {
    path: '',
    component: MisSolicitudesClientePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MisSolicitudesClientePage]
})
export class MisSolicitudesClientePageModule {}
