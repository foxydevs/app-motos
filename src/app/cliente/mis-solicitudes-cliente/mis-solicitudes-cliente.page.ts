import { Component, OnInit } from '@angular/core';
import { PujaService } from 'src/app/service/puja.service';
import { NotificacionService } from 'src/app/service/notificacion.service';
import * as momenttz from 'moment-timezone';
import * as moment from 'moment-timezone';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-mis-solicitudes-cliente',
  templateUrl: './mis-solicitudes-cliente.page.html',
  styleUrls: ['./mis-solicitudes-cliente.page.scss'],
})
export class MisSolicitudesClientePage implements OnInit {
  private navColor:String = '1763A6';
  private table:any[];

  constructor(private router: Router,
    private location:Location,
    private mainService: PujaService,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.getAll(+localStorage.getItem('currentId'))
  }

  
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  //GET ALL
  getAll(id: any) {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getFilter(id, 'cliente')
    .subscribe((res) => {
      this.table = [];
      for(let x of res) {
        let data = {
          tiempo: moment(moment.tz(new Date(x.fecha), 'America/Guatemala')).startOf('hour').fromNow(),
          recoger_en: x.recoger_en,
          llegar_a: x.llegar_a,
          id: x.id,
          state: x.state
        }
        this.table.push(data)
      }
      this.table.reverse();
      this.notificationService.dismiss();
    }, (error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }

}
