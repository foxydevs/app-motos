import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisSolicitudesClientePage } from './mis-solicitudes-cliente.page';

describe('MisSolicitudesClientePage', () => {
  let component: MisSolicitudesClientePage;
  let fixture: ComponentFixture<MisSolicitudesClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisSolicitudesClientePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisSolicitudesClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
