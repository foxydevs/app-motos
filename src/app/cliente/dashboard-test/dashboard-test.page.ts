import { Component, OnInit, NgZone } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ModalController, AlertController } from '@ionic/angular';
import { DireccionService } from 'src/app/service/direccion.service';
import { PujaService } from 'src/app/service/puja.service';
import { NotificacionService } from 'src/app/service/notificacion.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalMisDireccionesComponent } from '../dashboard-cliente/modal-mis-direcciones/modal-mis-direcciones.component';

//GOOGLE
declare var google;

@Component({
  selector: 'app-dashboard-test',
  templateUrl: './dashboard-test.page.html',
  styleUrls: ['./dashboard-test.page.scss'],
})
export class DashboardTestPage implements OnInit {
  private map: any;
  private search:any;
  private GoogleAutocomplete:any;
  private autocomplete:any;
  private autocompleteItems:any;
  private markers:any[];
  private waypts:any[];
  private geocoder:any;
  private directionsService: any;
  private directionsDisplay: any;
  private latitude = 0;
  private longitude = 0;
  private data = {
    total: '',
    cliente: localStorage.getItem('currentId'),
    taxi: '',
    recoger: '',
    llegar: '',
    state: 0
  }
  private addressOrigin = {
    direccion: '',
    usuario: localStorage.getItem('currentId'),
    tipo: 1,
    latitud: 0,
    longitud: 0,
    titulo: '',
    id: null
  }
  private addressDestination = {
    direccion: '',
    usuario: localStorage.getItem('currentId'),
    tipo: 1,
    latitud: 0,
    longitud: 0,
    titulo: '',
    id: null
  }
  private navColor:String = '1763A6';

  constructor(private geolocation: Geolocation,
    private router: Router,
    private location:Location,
    private zone: NgZone,
    private modalController: ModalController,
    private mainService: DireccionService,
    private alertController: AlertController,
    private secondService: PujaService,
    private notificationService :NotificacionService) {
    this.getLocation();
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];
    this.geocoder = new google.maps.Geocoder;
    this.markers = [];
    this.waypts = [];
  }

  ngOnInit() { }
  
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  getLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude
      this.longitude = resp.coords.longitude
      this.loadMap(resp.coords.latitude, resp.coords.longitude);
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }

  public loadMap(lat:any, long:any){  
    let mapEle: HTMLElement = document.getElementById('map');

    let myLatLng = {lat: lat, lng: long};

    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });
    this.getLocationByLatLng(lat, long);

    this.directionsDisplay.setMap(this.map);
    this.calculateRoute(lat, long);
    this.directionsDisplay = new google.maps.DirectionsRenderer({
      draggable: true,
      map: this.map,
      panel: document.getElementById('right-panel')
    });
    this.directionsDisplay.addListener('directions_changed', () => {
      this.getLocationByIDOrigin(this.directionsDisplay.getDirections().geocoded_waypoints[0].place_id);
      //this.getLocationByIDDestination(this.directionsDisplay.getDirections().geocoded_waypoints[1].place_id)
      this.computeTotalDistance(this.directionsDisplay.getDirections());
    });
/*
    var marker:any;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.latitude = evt.latLng.lat();
      this.longitude = evt.latLng.lng();
      this.geocoder.geocode({'placeId': marker.place_id}, (results:any, status:any) => {
        if(status === 'OK' && results[0]){
          let position = {
              lat: results[0].geometry.location.lat(),
              lng: results[0].geometry.location.lng()
          };
        }
      })
    });*/

    google.maps.event.addListenerOnce(this.map, 'dragend', (res) => {
      console.log("A CADA RATO")
      this.latitude = res.latLng.lat();
      this.longitude = res.latLng.lng();
      new google.maps.Marker({
        position: myLatLng,
        map: this.map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        title: 'Mi Ubicación'
      });
      mapEle.classList.add('show-map');
    });
  }

  public computeTotalDistance(result:any) {
    var total = 0;
    var myroute = result.routes[0];
    for (var i = 0; i < myroute.legs.length; i++) {
      total += myroute.legs[i].distance.value;
    }
    total = total / 1000;
    console.log(total + "KM")
  }

  updateSearchResults(){
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
    (predictions, status) => {
      this.autocompleteItems = [];
      this.zone.run(() => {
        console.log(predictions)
        predictions.forEach((prediction) => {
          this.autocompleteItems.push(prediction);
        });
      });
    });
  }

  selectSearchResult(item:any){
    this.autocompleteItems = [];
    console.log(item)
    this.addressDestination.direccion = item.description;
    this.addressDestination.titulo = item.description;
    this.geocoder.geocode({'placeId': item.place_id}, async (results:any, status:any) => {
      if(status === 'OK' && results[0]){
        
        let position = {
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng()
        };
        let data2 = {
          location: {lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng()},
          stopover: true,
        }
        const alert = await this.alertController.create({
          header: 'Confirm!',
          message: 'Message <strong>text</strong>!!!',
          buttons: [
            {
              text: 'Cambiar Destino',
              cssClass: 'secondary',
              handler: (blah) => {
                console.log('Confirm Cancel: blah');
                this.calculateRoute(position.lat, position.lng);
                this.addressDestination.latitud = position.lat;
                this.addressDestination.longitud = position.lng;
              }
            }, {
              text: 'Agregar Nuevo Puntero',
              handler: () => {
                console.log('Confirm Okay');
                this.waypts.push(data2)
              }
            }
          ]
        });
    
        await alert.present();
        
        
        /*let marker = new google.maps.Marker({
          position: results[0].geometry.location,
          map: this.map,
        });
        this.markers.push(marker);
        this.map.setCenter(results[0].geometry.location);*/
      }
    })
  }

  public calculateRoute(lat:any, lng:any){
    this.directionsService.route({
      origin: new google.maps.LatLng(this.latitude, this.longitude),
      destination: new google.maps.LatLng(lat, lng),
      optimizeWaypoints: true,
      waypoints: this.waypts,
      travelMode: google.maps.TravelMode.DRIVING,
      avoidTolls: true
    }, (response:any, status:any)=> {
      if(status === google.maps.DirectionsStatus.OK) {
        this.directionsDisplay.setDirections(response);
      }else{
        console.log('Could not display directions due to: ' + status);
      }
    });
  }

  public clearMarkers() {
    this.setMapOnAll(null);
  }

  public setMapOnAll(map:any) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  public getLocationByLatLng(lat:any, lng:any) {
    var latlng = {lat: lat, lng: lng};
    this.geocoder.geocode({'location': latlng}, (results:any, status:any) => {
      if(status === 'OK' && results[0]){
        this.addressDestination.direccion = results[0].formatted_address;
        this.addressDestination.titulo = results[0].formatted_address;
        this.addressDestination.latitud = results[0].formatted_address;
        this.addressDestination.longitud = results[0].formatted_address;
      }
    });
  }

  public getLocationByIDOrigin(place_id:any) {
    this.geocoder.geocode({'placeId': place_id}, (results:any, status:any) => {
      if(status === 'OK' && results[0]){
        console.log(results[0])
        this.addressOrigin.direccion = results[0].formatted_address;
        this.addressOrigin.titulo = results[0].formatted_address;
        this.addressOrigin.latitud = results[0].geometry.location.lat();
        this.addressOrigin.longitud = results[0].geometry.location.lng();
      }
    });
  }

  public getLocationByIDDestination(place_id:any) {
    this.geocoder.geocode({'placeId': place_id}, (results:any, status:any) => {
      if(status === 'OK' && results[0]){
        console.log(results[0])
        this.addressDestination.direccion = results[0].formatted_address;
        this.addressDestination.titulo = results[0].formatted_address;
        this.addressDestination.latitud = results[0].formatted_address;
        this.addressDestination.longitud = results[0].formatted_address;
      }
    });
  }

  //ABRIR MODAL
  async presentModal(parameter?:number) {
    const modal = await this.modalController.create({
      component: ModalMisDireccionesComponent,
      componentProps: { 
        value: parameter
      }
    });
    modal.onDidDismiss().then((data) => {
      console.log(data)
      this.calculateRoute(data.data.latitud, data.data.longitud);
      this.addressDestination.direccion = data.data.titulo;
      this.addressDestination.id = data.data.id;
      this.data.llegar = data.data.id;
    });
    return await modal.present();
  }

  saveChanges() {
    if(this.data.total) {
      console.log('ORIGIN', this.addressOrigin)
      console.log('DESTINATION', this.addressDestination)
      if(!this.addressOrigin.id) {
        this.createAddressOrigin();
      }
      if(!this.addressDestination.id) {
        this.createAddressDestination();
      }
      setTimeout(() => {
        console.log('PUJA', this.data)
        this.create();
        this.notificationService.dismiss()
      }, 1000);
    } else {
      this.notificationService.alertToast('La tarifa es requerida, asegurese de que sea un precio razonable.');
    }
    /*if(this.data.recoger) {
      if(this.data.llegar) {
        if(this.data.total) {
          this.notificationService.alertLoading('Procesando...', 100000);
          
        } else {
          this.notificationService.alertToast('La tarifa es requerida, asegurese de que sea un precio razonable.');
        }
      } else {
        this.notificationService.alertToast('El lugar de destrino es requerido.');
      }
    } else {
      this.notificationService.alertToast('El lugar de partida es requerido.');
    }*/
  }

  //CREATE ORIGIN
  createAddressOrigin() {
    this.mainService.create(this.addressOrigin)
    .subscribe((res) => {
      this.addressOrigin.id = res.id;
      this.data.recoger = res.id;
    }, (error) => {
      console.log(this.addressOrigin);
      console.error(error);
    });
  }

  //CREATE DESTINATION
  createAddressDestination() {
    this.mainService.create(this.addressDestination)
    .subscribe((res) => {
      this.addressDestination.id = res.id;
      this.data.llegar = res.id;
    }, (error) => {
      console.log(this.addressDestination);
      console.error(error);
    });
  }

  //CREATE PUJA
  create() {
    this.secondService.create(this.data)
    .subscribe((res) => {
      this.notificationService.alertMessage('Pedido en Proceso', 'Espere un momento en lo que un conductor responde a su solicitud.');
      this.goToRoute('cliente/mis-solicitudes-cliente');
    }, (error) => {
      console.log(this.data);
      console.error(error);
    });
  }
}
