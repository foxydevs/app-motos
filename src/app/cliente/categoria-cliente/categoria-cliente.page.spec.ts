import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriaClientePage } from './categoria-cliente.page';

describe('CategoriaClientePage', () => {
  let component: CategoriaClientePage;
  let fixture: ComponentFixture<CategoriaClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriaClientePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriaClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
