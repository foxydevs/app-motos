import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-categoria-cliente',
  templateUrl: './categoria-cliente.page.html',
  styleUrls: ['./categoria-cliente.page.scss'],
})
export class CategoriaClientePage implements OnInit {
  private navColor:String = '1763A6';
  parameter:any;
  title:string;
  categorias = [
    {
      id: 1,
      imagen: 'http://www.maefotografos.net/bmz_cache/4/40aa499104fd121c5966f9ac50992776.image.500x500.jpg',
      nombre: "Ropa"
    },
    {
      id: 2,
      imagen: 'https://www.fisa.com.co/components/com_mijoshop/opencart/image/cache/catalog/Herr.Automotriz/Juego_Herramientas_Mantenimiento_911G3-500x500.jpg',
      nombre: "Herramientas"
    },
    {
      id: 3,
      imagen: 'http://www.libreriasanchez.com/wp-content/uploads/2017/12/libreria-sanchez-clientes-01-500x500.jpg',
      nombre: "Libros"
    },
  ]
  restaurantes = [
    {
      id: 5,
      imagen: 'https://hippoprod.s3.amazonaws.com/merchants/logos/000/000/046/large/mcdonalds_india_logo.png?1461339852',
      nombre: "McDonald's"
    },
    {
      id: 6,
      imagen: 'https://i.pinimg.com/originals/e0/fa/20/e0fa203d3777ac9887552e23bf782484.jpg',
      nombre: "Pollo Campero"
    },
    {
      id: 7,
      imagen: 'https://fastly.4sqi.net/img/general/600x600/137737769_jFkxGWwbiy55TZ2WM_rdbHFksvwNFFT3NvDE0CWfSHI.jpg',
      nombre: "Dominos Pizza"
    },
    {
      id: 8,
      imagen: 'http://www.brandemia.org/sites/default/files/inline/images/logo_taco_bell_despues1.jpg',
      nombre: "Taco Bell"
    }
  ]

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location) {
    this.parameter = this.activatedRoute.snapshot.paramMap.get('type');
    console.log(this.parameter)
    if(this.parameter=='categoria') {
      this.title = "Categoría";      
    } else if(this.parameter=='restaurante') {
      this.title = "Restaurantes";      
    }
  }

  ngOnInit() {
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }


}
