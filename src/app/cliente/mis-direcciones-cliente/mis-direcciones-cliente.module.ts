import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MisDireccionesClientePage } from './mis-direcciones-cliente.page';

const routes: Routes = [
  {
    path: '',
    component: MisDireccionesClientePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MisDireccionesClientePage]
})
export class MisDireccionesClientePageModule {}
