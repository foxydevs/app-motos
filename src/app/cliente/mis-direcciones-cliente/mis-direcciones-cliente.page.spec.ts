import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisDireccionesClientePage } from './mis-direcciones-cliente.page';

describe('MisDireccionesClientePage', () => {
  let component: MisDireccionesClientePage;
  let fixture: ComponentFixture<MisDireccionesClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisDireccionesClientePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisDireccionesClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
