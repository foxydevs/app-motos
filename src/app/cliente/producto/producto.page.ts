import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.page.html',
  styleUrls: ['./producto.page.scss'],
})
export class ProductoPage implements OnInit {
  private navColor:String = '1763A6';
  private parameter:any;
  productos = [
    {
      nombre: "Pantalon",
      precio: 200,
      imagen: "https://statics.glamit.com.ar/media/catalog/product/cache/8/image/9df78eab33525d08d6e5fb8d27136e95/g/r/grisino_g1pe08_nh_1.jpg",
      categoria: 1
    },
    {
      nombre: "Camisa",
      precio: 90,
      imagen: "https://images-na.ssl-images-amazon.com/images/I/61JFNe07SPL._UX679_.jpg",
      categoria: 1
    },
    {
      nombre: "Zapatos",
      precio: 125,
      imagen: "https://http2.mlstatic.com/botas-bebe-invierno-calzado-zapatos-nino-frio-senderismo-D_NQ_NP_744049-MLM31217812025_062019-F.jpg",
      categoria: 1
    },
    {
      nombre: "Desarmador",
      precio: 10,
      imagen: "https://static.grainger.com/rp/s/is/image/Grainger/41ZN98_AS01?$glgmain$",
      categoria: 2
    },
    {
      nombre: "Martillo",
      precio: 15,
      imagen: "https://ferreteriavidri.com/images/items/large/9781.jpg",
      categoria: 2
    },
    {
      nombre: "Casco",
      precio: 20,
      imagen: "https://http2.mlstatic.com/casco-de-seguridad-industrial-obrero-ingeniero-contratista-D_NQ_NP_907866-MCO26015925131_092017-Q.jpg",
      categoria: 2
    },
    {
      nombre: "Harry Potter y la Pieda Filosofal",
      precio: 200,
      imagen: "https://imagessl2.casadellibro.com/a/l/t0/52/9788478884452.jpg",
      categoria: 3
    },
    {
      nombre: "Harry Potter y la Camara de los Secretos",
      precio: 200,
      imagen: "https://s3.amazonaws.com/librero/books/nueva-portada/portada-libro-harry-potter-y-el-misterio-del-principe-9788498384482-jk-rowling-9788498384482-bogota-librero.jpg",
      categoria: 3
    },
    {
      nombre: "Harry Potter y el Prisionero de Askaban",
      precio: 200,
      imagen: "https://s3.amazonaws.com/librero/books/nueva-portada/portada-libro-harry-potter-y-la-orden-del-fenix-9788498384468-jk-rowling-9788498384468-bogota-librero.jpg",
      categoria: 3
    },
    {
      nombre: "Big Mac",
      precio: 40,
      imagen: "https://www.mygiftstohome.com/image/cache/catalog/chicken-big-mac-from-mcdonalds-471-500x500.jpg",
      categoria: 5
    },
    {
      nombre: "Macnífica",
      precio: 40,
      imagen: "https://images.deliveryhero.io/image/pedidosya/products/393843-b1cc82f0-7aa8-4751-a4e2-b895c61da813.jpg?quality=80",
      categoria: 5
    },
    {
      nombre: "Alitas",
      precio: 120,
      imagen: "https://www.campero.com/iCadImagesMNC/Productos/W30AlitEmp.png",
      categoria: 6
    },
    {
      nombre: "Tradicional",
      precio: 30,
      imagen: "https://www.guatemala.com/fotos/2019/05/g2.jpg",
      categoria: 6
    },
    {
      nombre: "5 Carnes",
      precio: 30,
      imagen: "http://www.three.co.uk/hub/wp-content/uploads/Dominos.jpg",
      categoria: 7
    },
    {
      nombre: "Deluxe",
      precio: 30,
      imagen: "http://www.three.co.uk/hub/wp-content/uploads/Dominos.jpg",
      categoria: 7
    },
    {
      nombre: "Hawaina",
      precio: 30,
      imagen: "http://www.three.co.uk/hub/wp-content/uploads/Dominos.jpg",
      categoria: 7
    },
    {
      nombre: "Combo 1",
      precio: 30,
      imagen: "https://i0.wp.com/www.veganlyapp.com/wp-content/uploads/2019/05/taco-bell-uk-launches-vegan-meat-made.jpg?fit=500%2C500",
      categoria: 8
    },
    {
      nombre: "Combo 2",
      precio: 30,
      imagen: "https://i0.wp.com/www.veganlyapp.com/wp-content/uploads/2019/05/taco-bell-uk-launches-vegan-meat-made.jpg?fit=500%2C500",
      categoria: 8
    },
  ]

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location) {
      this.parameter = this.activatedRoute.snapshot.paramMap.get('id');
    }

  ngOnInit() {
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  agregarCarrito(e:any) {
    let data = {
      cantidad: 1,
      producto: e,
    }
    let carts:any[] = [];
    carts = JSON.parse(localStorage.getItem('currentCarrito'));
    carts.push(data);
    localStorage.removeItem('currentCarrito');
    localStorage.setItem('currentCarrito', JSON.stringify(carts));
  }
}
